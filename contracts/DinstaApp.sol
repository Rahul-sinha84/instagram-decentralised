pragma solidity ^0.5.16;

contract DinstaApp {
    string public name = "DinstaApp";
    uint private imageCount;
    //event for creation of image
    event ImageCreated (
        uint id,
        string hash,
        string description,
        uint tipAmount,
        address payable author
    );
    //event for tipping the image
    event ImageTipped (
        uint id,
        address payable author,
        address sender,
        uint amount
    );
    
    //store images
    struct Image {
        uint id;
        string hash;
        string description;
        uint tipAmount;
        address payable author;
    }

    mapping(uint => Image) private images;

    //get images
    function getImage(uint _index) public view returns(
        uint, 
        string memory, 
        string memory, 
        uint, 
        address payable
    ) {
        return (
            images[_index].id, 
            images[_index].hash,
            images[_index].description,
            images[_index].tipAmount,
            images[_index].author
        );
    }

    //get image count
    function getImageCount() public view returns(uint) {
        return imageCount;
    }
    
    
    //create images
    function uploadImage( 
        string memory _hash, 
        string memory _description
    ) public {
        // validation
        require(msg.sender != address(0x0), "Invalid Address");
        require(bytes(_hash).length > 0, "Hash can't be empty");
        require(bytes(_description).length > 0, "Description can't be empty");
        imageCount++;
        images[imageCount] = Image(imageCount, _hash, _description, 0, msg.sender);
        //emitting the image creation event
        emit ImageCreated(
            images[imageCount].id, 
            images[imageCount].hash, 
            images[imageCount].description, 
            images[imageCount].tipAmount, 
            images[imageCount].author
        );
    }

    //tip images
    function tipImageOwner(uint _index) public payable {
        //valid id
        require(_index > 0 && _index <= imageCount, "Invalid image");
        Image memory _image = images[_index];
        //sending funds to author of the image
        address(_image.author).transfer(msg.value);
        //incrementing the funds
        _image.tipAmount += msg.value;
        //updating the image
        images[_index] = _image;

        //emitting the event
        emit ImageTipped(_index, _image.author, msg.sender, msg.value);
    }
}