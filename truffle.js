const path = require("path");
require("babel-register");
require("babel-polyfill");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  contracts_build_directory: path.join(__dirname, "client/src/contracts"),
  networks: {
    develop: {
      port: 8545,
      network_id: "*",
      host: "localhost",
    },
    ganache: {
      port: 7545,
      network_id: "*",
      host: "localhost",
    },
  },
  compilers: {
    solc: {
      version: "0.5.16",
    },
  },
};
