import React, { useState } from "react";
import Identicon from "identicon.js";

const Main = ({ uploadImage, captureFile, images, tipImageOwner, web3 }) => {
  const [description, setDescription] = useState("");
  const onSubmit = (e) => {
    e.preventDefault();
    uploadImage(description);
  };
  const returnImages = () => {
    return images.map((val, ind) => (
      <div className="card mb-4" key={ind}>
        <div className="card-header">
          <img
            src={`data:image/png;base64,${new Identicon(
              val[4],
              30
            ).toString()}`}
            alt="profile-photo"
            className="mr-2"
            width="30"
            height="30"
          />
          <small className="text-muted">{val[4]}</small>
        </div>
        <ul className="list-group list-group-flush" id="imageList">
          <li className="list-group-item">
            <p className="text-center">
              <img
                style={{ width: "100%" }}
                src={`https://ipfs.infura.io/ipfs/${val[1]}`}
                alt="image"
              />
              <p>{val[2]}</p>
            </p>
          </li>
          <li className="list-group-item py-2" key={ind}>
            <small className="float-left mt-1 text-muted">
              TIPS: {web3.utils.fromWei(`${val[3]}`, "ether")} ETH
            </small>
            <button
              className="btn btn-link btn-sm float-right pt-0"
              name={val[0]}
              onClick={(e) => {
                console.log(e);
                let tipAmount = web3.utils.toWei("0.1", "ether");
                console.log(e.target.name, tipAmount);
                tipImageOwner(e.target.name, tipAmount);
              }}
            >
              TIP 0.1 ETH
            </button>
          </li>
        </ul>
      </div>
    ));
  };
  return (
    <div className="container-fluid mt-5">
      <div className="row">
        <main
          role="main"
          className="col-lg-12 ml-auto mr-auto"
          style={{ maxWidth: "500px" }}
        >
          <div className="content mr-auto ml-auto">
            <p>&nbsp;</p>
            <h2>Share Image</h2>
            <form onSubmit={onSubmit}>
              <input type="file" onChange={captureFile} />
              <div className="form-group mr-sm-2">
                <br />
                <input
                  type="text"
                  className="form-control"
                  onChange={(e) => setDescription(e.target.value)}
                  value={description}
                  placeholder="Image Description..."
                  required
                />
              </div>
              <button className="btn btn-primary btn-block btn-lg">
                Upload!
              </button>
            </form>
            <p>&nbsp;</p>

            {/* Code ... */}
            {returnImages()}
          </div>
        </main>
      </div>
    </div>
  );
};

export default Main;
