import React, { useState, useEffect } from "react";
import getWeb3 from "./getWeb3";
import DinstaApp from "../contracts/DinstaApp.json";
import Identicon from "identicon.js";
import "./App.css";
import Navbar from "./Navbar";
import Main from "./Main";

const ipfsClient = require("ipfs-http-client");
//connecting with the ipfs mainnet
const ipfs = ipfsClient.create({
  host: "ipfs.infura.io",
  port: 5001,
  protocol: "https",
});
console.log(ipfsClient);

const App = () => {
  const [account, setAccount] = useState("");
  const [web3, setWeb3] = useState(null);
  const [loading, setLoading] = useState(false);
  const [contract, setContract] = useState(null);
  const [buffer, setBuffer] = useState([]);
  const [images, setImages] = useState([]);
  useEffect(() => {
    (async () => {
      const web3Instance = await getWeb3();
      setWeb3(web3Instance);
      await loadBlockchainData(web3Instance);
    })();
  }, []);
  const loadBlockchainData = async (web3) => {
    const accounts = await web3.eth.getAccounts();
    setAccount(accounts[0]);
    //getting network id from ganache
    const networkId = await web3.eth.net.getId();
    //getting network data from abi
    const networkData = DinstaApp.networks[networkId];
    if (networkData) {
      //getting the contract data from abi
      const contractData = await new web3.eth.Contract(
        DinstaApp.abi,
        networkData.address
      );
      setContract(contractData);
      //getting images
      getImages(contractData);
    } else {
      window.alert("DinstaApp contract is not deployed to detected network !!");
    }
  };
  //fetching images from blockchain
  const getImages = async (contractData) => {
    //getting total image count in the blockchain
    const imageCount = await contractData.methods.getImageCount().call();
    console.log(imageCount);
    let reqImages = [];
    //getting all images attributes from blockchain
    for (let i = 1; i <= imageCount; i++) {
      const image = await contractData.methods.getImage(i).call();
      reqImages.push(image);
    }
    setImages(reqImages.sort((a, b) => b[3] - a[3]));
    setLoading(true);
  };
  //creating buffer of the image file to store it in IPFS
  const captureFile = (e) => {
    e.preventDefault();
    const file = e.target.files[0];
    const reader = new window.FileReader();
    //converting the file to buffer
    //as buffer data is required to store
    //in ipfs
    reader.readAsArrayBuffer(file);
    reader.onloadend = () => {
      setBuffer(Buffer(reader.result));
    };
  };
  //uploading the image to IPFS, and then to blockchain
  const uploadImage = (description) => {
    console.log("submitting file to IPFS...");

    //adding file to IPFS
    ipfs
      .add(buffer)
      .then(async (result) => {
        console.log("ipfs result", result);

        setLoading(false);
        //uploading it on blockchain
        await contract.methods
          .uploadImage(result.path, description)
          .send({ from: account })
          .on("transactionHash", (hash) => {
            setLoading(true);
            //reloading the images
            getImages(contract);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const tipImageOwner = async (index, amount) => {
    setLoading(false);
    await contract.methods
      .tipImageOwner(index)
      .send({ from: account, value: amount })
      .on("transactionHash", (hash) => {
        setLoading(true);
        //reloading the images
        getImages(contract);
      });
  };
  return (
    <div>
      <Navbar account={account} />
      {!loading ? (
        <div id="loader" className="text-center mt-5">
          <p>Loading...</p>
        </div>
      ) : (
        <Main
          images={images}
          captureFile={captureFile}
          uploadImage={uploadImage}
          web3={web3}
          tipImageOwner={tipImageOwner}
        />
      )}
    </div>
  );
};

export default App;
