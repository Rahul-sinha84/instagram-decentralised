import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import chaiAsBN from "chai-bn";
const BN = web3.utils.BN;
const chaiBN = chaiAsBN(BN);
chai.use(chaiBN);
chai.use(chaiAsPromised);

export default chai;
