import chai from "./setupChai";
import etherToWei from "./helper/etherToWei";
import getBalance from "./helper/getBalance";
const DinstaApp = artifacts.require("DinstaApp.sol");

const expect = chai.expect;
const BN = web3.utils.BN;

contract("DinstaApp contract", async (accounts) => {
  const [deployerAccount, recepient, anotherAccount] = accounts;
  const deployedName = "DinstaApp";
  let dinstaApp;
  beforeEach(async () => {
    dinstaApp = await DinstaApp.deployed();
  });
  describe("Deployment", () => {
    it("deployed successfully", async () => {
      expect(dinstaApp.address).to.not.equal("0x0");
      expect(dinstaApp.address).to.not.equal("");
      expect(dinstaApp.address).to.not.equal(null);
      return expect(dinstaApp.address).to.not.equal(undefined);
    });
    it("has a name", async () => {
      return expect(dinstaApp.name()).to.eventually.be.equal(deployedName);
    });
  });
  describe("Image", () => {
    let result, imageCount, event;
    const _hash = "http://rahulsinha.xyz";
    const _description = "This is my portfolio";
    before(async () => {
      result = await dinstaApp.uploadImage(_hash, _description, {
        from: recepient,
      });
      imageCount = await dinstaApp.getImageCount();
      event = result.logs[0].args;
    });
    it("successful image upload...", async () => {
      expect(event["hash"], "hash is correct").to.be.equal(_hash);
      expect(event["description"], "dexcription is correct").to.be.equal(
        _description
      );
      expect(event["id"], "id is correct").to.be.a.bignumber.equal(imageCount);
      expect(
        event["tipAmount"],
        "tip amount is correct"
      ).to.be.a.bignumber.equal(new BN(0));
      expect(event["author"], "author is correct").to.be.equal(recepient);
      return expect(imageCount).to.be.a.bignumber.equal(new BN(1));
    });
    it("unsuccessful image upload...", async () => {
      expect(dinstaApp.uploadImage("", _description), "for hash").to.eventually
        .be.rejected;
      return expect(dinstaApp.uploadImage(_hash, ""), "for description").to
        .eventually.be.rejected;
    });
    it("tipping the image", async () => {
      const authorBal = await getBalance(recepient);
      const senderBal = await getBalance(anotherAccount);
      const tippedAmount = 0.005;
      const transaction = await dinstaApp.tipImageOwner(imageCount, {
        from: anotherAccount,
        value: etherToWei(tippedAmount),
      });
      const emittedEvent = transaction.logs[0].args;
      //should not process for invalid image number
      expect(
        dinstaApp.tipImageOwner(2, {
          from: anotherAccount,
          value: etherToWei(2),
        })
      ).to.eventually.be.rejected;
      //should target the correct image
      expect(emittedEvent.id).to.be.a.bignumber.equal(new BN(imageCount));
      //author should be correct
      expect(emittedEvent.author).to.be.equal(recepient);
      //sender should be correct
      expect(emittedEvent.sender).to.be.equal(anotherAccount);
      //amount should be correct
      expect(emittedEvent.amount).to.be.a.bignumber.equal(
        new BN(etherToWei(tippedAmount))
      );
      //sender's balance should be deducted
      expect(getBalance(emittedEvent.sender)).to.eventually.be.not.equal(
        senderBal
      );
      //author should be credited
      return expect(getBalance(emittedEvent.author)).to.eventually.be.equal(
        `${+authorBal + +emittedEvent.amount}`
      );
    });
  });
});
