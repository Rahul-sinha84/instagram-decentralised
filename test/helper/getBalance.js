export default async (account) => {
  return web3.eth.getBalance(account);
};
