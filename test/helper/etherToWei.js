// const BN = web3.utils.BN;
export default (amount) => {
  return web3.utils.toWei(`${amount}`, "ether");
};
